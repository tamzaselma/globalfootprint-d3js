var myGeoJSONPath = "world.geo.json";
var countryGeoData;
var maplayer;

var myCustomStyle = {
  weight: 1.5,
  fill: true,
  fillColor: "#fff",
  fillOpacity: 1
};

function onEachFeature(feature, layer) {
  layer.on({
    click: function(e) {
      console.log(e.target.feature.properties.admin);
    }
  });
}

d3.json(myGeoJSONPath).then(function(data) {
  countryGeoData = data;
  var map = L.map("map").setView([30, 45], 1.5);

  maplayer = L.geoJson(data.features, {
    clickable: false,
    style: myCustomStyle,
    onEachFeature: onEachFeature
  });
  maplayer.addTo(map);
});

document.addEventListener("DOMContentLoaded", () => {
  loadCSVData("2016-ecological-footprint.csv").then(countryFootprintData => {
    const worldMap = new WorldMap(
      "map",
      "2016-ecological-footprint.csv",
      countryFootprintData
    );
    worldMap.draw();

    const navBar = new NavBar(
      "Footprints",
      [
        //TODO better load data from the dataset
        { name: "L'empreinte des Champs Cultivés", data: "Cropland Footprint" },
        { name: "L'empreinte des Pâturages ", data: "Grazing Footprint" },
        { name: "L'empreinte des Forêts", data: "Forest Footprint" },
        { name: "L'empreinte carbone", data: "Carbon Footprint" },
        { name: "L'empreinte des Pêcheries", data: "Fish Footprint" },
        {
          name: "L’empreinte Globale",
          data: "Total Ecological Footprint",
          default: true
        }
      ],
      worldMap
    );
    navBar.draw();
  });
});

class NavBar {
  constructor(title, buttons, target) {
    var self = this;
    this.title = title;
    this.buttons = buttons;
    this.target = target;
  }

  draw() {
    this.navbarContainer = document.getElementById("navbar");
    this.buttons.forEach(this.setButton, this);
  }

  setButton(button) {
    var self = this;
    var newButton = document.createElement("button");
    newButton.setAttribute("id", button.name);
    newButton.setAttribute("value", button.data);
    newButton.setAttribute("type", "button");
    newButton.innerHTML = button.name;
    newButton.onclick = function() {
      self.displayData(this);
    };
    newButton.classList.add("button", "-blue", "center");
    if (button.default) {
      this.activateButton(newButton);
    }
    this.navbarContainer.appendChild(newButton);
  }

  displayData(btn) {
    this.clearActiveButtons();
    this.activateButton(btn);
    this.target.draw(btn.getAttribute("value"));
  }

  clearActiveButtons() {
    var elems = document.querySelectorAll(".button.active");
    elems.forEach((btn) => {
      btn.classList.remove("active");
      btn.classList.add("-blue");
    });
  }

  activateButton(btn) {
    btn.classList.remove("-blue");
    btn.classList.add("active");
  }
}

class WorldMap {
  constructor(id, dataset, countryFootprintData) {
    this.dataset = dataset;
    this.svg = d3.select(
      document.getElementById(id).getElementsByTagName("svg")[0]
    );
    this.countryFootprintData = countryFootprintData; //TODO to remove - should be loaded inside the class

    this.propertyValueRangeMap = [];
    this.propertyValueRangeMap["Cropland Footprint"] = [
      0.33,
      0.59,
      0.85,
      1.11,
      1.38,
      1.64,
      1.9,
      2.42
    ];
    this.propertyValueRangeMap["Grazing Footprint"] = [0.35, 0.69, 1.04, 1.39];
    this.propertyValueRangeMap["Forest Footprint"] = [
      0.31,
      0.61,
      0.92,
      1.22,
      1.52
    ];
    this.propertyValueRangeMap["Carbon Footprint"] = [
      1.27,
      2.53,
      3.8,
      5.06,
      6.33,
      7.59
    ];
    this.propertyValueRangeMap["Fish Footprint"] = [
      0.08,
      0.16,
      0.25,
      0.33,
      0.41,
      0.49,
      0.66,
      0.74
    ];
    this.propertyValueRangeMap["Total Ecological Footprint"] = [
      1.96,
      3.5,
      5.04,
      6.58,
      8.12,
      9.66,
      11.2,
      12.74
    ];

    this.hues = [
      "#fcfbfd",
      "#efedf5",
      "#dadaeb",
      "#bcbddc",
      "#9e9ac8",
      "#807dba",
      "#6a51a3",
      "#54278f",
      "#3f007d"
    ];
  }

  draw(category) {
    var self = this;
    this.data = this.joinCountryData(
      countryGeoData.features,
      this.countryFootprintData
    );
    const color = d3.scaleOrdinal(d3.schemeDark2);
    if (!category) {
      category = "Total Ecological Footprint";
    }

    maplayer.eachLayer(function(layer) {
      layer.setStyle({
        fillColor: self.getColor(layer, category),
        fillOpacity: 0.8,
        weight: 1.5
      });
    });
    self.drawMapLegend(category);
  }

  getColor(layer, category) {
    var self = this;
    var d = layer.feature.footprint[category];
    var ranges = self.propertyValueRangeMap[category];

    var color = self.hues[self.hues.length - ranges.length - 1];
    ranges.some((value, index) => {
      if (d < value) {
        color = self.hues[self.hues.length - ranges.length + index];
        return true;
      } else {
        return false;
      }
    });

    return color;
  }

  drawMapLegend(category) {
    var self = this;
    var ranges = self.propertyValueRangeMap[category];

    var ul = document.createElement("ul");
    var text1 = document.createTextNode("ha/personne");
    ul.appendChild(text1);
    ranges.forEach((range, index) => {
      var span = document.createElement("span");
      span.style.backgroundColor =
        self.hues[self.hues.length - ranges.length + index];
      var li = document.createElement("li");
      var text = document.createTextNode("> " + range.toFixed(2));
      li.appendChild(span);
      li.appendChild(text);
      ul.appendChild(li);
    });

    var legendContainer = document.getElementById("map-legend");

    while (legendContainer.firstChild) {
      legendContainer.removeChild(legendContainer.firstChild);
    }
    legendContainer.appendChild(ul);
  }

  joinCountryData(countryGeoDataSet, countryFootprintDataSet) {
    maplayer.eachLayer(function(layer) {
      var countryFootprintData = Object.assign(
        {},
        countryFootprintDataSet.find(
          countryData =>
            layer.feature.properties.admin == countryData["Country"]
        )
      );
      layer.feature.footprint = countryFootprintData;
    });

    var joinedCountryDataSet = countryFootprintDataSet.reduce(
      (result, currentF) => {
        var fullCountryData = Object.assign(
          {},
          currentF,
          countryGeoDataSet.find(
            currentG => currentG.properties.admin == currentF["Country"]
          )
        );
        return result;
      },
      []
    );
    return joinedCountryDataSet;
  }
}

function loadCSVData(path) {
  return d3.csv(path);
}
