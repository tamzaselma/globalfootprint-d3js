// Premiere option
var value = 1;

const nbHabitantMin = 10;
const nbHabitantMax = 1;
const DEFAULT_ACTIVE_BUTTON_ID = 'button1';

var margin = { top: 20, right: 20, bottom: 70, left: 40 },
  width = 1000 - margin.left - margin.right,
  height = 300 - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width], 0.05);

var y = d3.scaleLinear().range([height, 0]);
var xAxis = d3.axisBottom().scale(x);

var yAxis = d3
  .axisLeft()
  .scale(y)
  .ticks(5);
function changeData(btn) {
  clearActiveButtons();
  activateButton(btn);
  document.getElementsByTagName("chart")[0].innerHTML = "";
  updateData(btn.id);
}

function clearActiveButtons() {
  var elems = document.querySelectorAll(".button.active");
  elems.forEach((btn) => {
    btn.classList.remove("active");
    btn.classList.add("-blue");
  });
}

function activateButton(btn) {
  btn.classList.remove("-blue");
  btn.classList.add("active");
}

function updateData(value) {
  console.log(value);

  var lineFunction = d3.line()
    .x(function (d) { return d; })
    .y(function (d) { return (-0.00008*d.y + 2); })
    ;

  var svg = d3
    .select("chart")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  d3.csv("2016-ecological-footprint.csv").then(function (data) {
    var i = 0;
    data.forEach(d => {
      i++;
      d["GDP per Capita"] = d["GDP per Capita"]
        .replace("$", "")
        .replace(",", "")
        .replace(".", ",");
    });

    // Trier les donnees dans l'order decroissant
    data.sort(
      (a, b) => parseInt(b["GDP per Capita"]) - parseInt(a["GDP per Capita"])
    );

    x.domain(
      data
        .filter(d => {
          if (value === 'button1') {
            return d["Population (millions)"] >= nbHabitantMin;
          } else if (value === 'button2') {
            return d["Population (millions)"] <= nbHabitantMax;
          } else {
            return false;
          }
        })
        .map(d => {
          return d.Country;
        })
    );
    // Determiner l'élément maximum de l'array, prend en compte les filtres données
    y.domain([
      0,
      d3.max(data, function (d) {
        return arrayMax(
          data
            .map(function (d) {
              return d["Total Ecological Footprint"];
            })
            .map(Number)
        );
      })
    ]);




    svg
      .append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", "-.55em")
      .attr("transform", "rotate(-90)");

    svg
      .append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Value");

    svg
      .selectAll("bar")
      .data(
        data.filter(function (d) {
          if (value === 'button1') {
            return d["Population (millions)"] >= nbHabitantMin;
          } else if (value === 'button2') {
            return d["Population (millions)"] <= nbHabitantMax;
          } else {
            return false;
          }
        })
      )


      .enter()
      .append("rect")
      //Choix de la couleur en fonction de la valeur
      .attr("fill", function (d) {
        if (parseInt(d["GDP per Capita"]) <= 1000) {
          return "#a1c45a";
        } else if (parseInt(d["GDP per Capita"]) < 10000) {
          return "#7971ea";
        } else if (parseInt(d["GDP per Capita"]) < 50000) {
          return "#f1c550";
        } else if (parseInt(d["GDP per Capita"]) >= 50000) {
          return "#ea4c4c";
        }
        return "black";
      })
      .attr("x", function (d) {
        return x(d.Country);
      })
      .attr("width", x.bandwidth())
      .attr("y", function (d) {
        return y(d["Total Ecological Footprint"]);
      })
      .attr("height", function (d) {
        return height - y(d["Total Ecological Footprint"]);
      });


      svg
      .append("path")
      .attr("d", lineFunction(value["GDP per Capita"]))
      .attr("stroke", "blue")
      .attr("stroke-width", 2)
      .attr("fill", "none");;


  });
}
document.addEventListener("DOMContentLoaded", function (event) {
  updateData(DEFAULT_ACTIVE_BUTTON_ID);
  activateButton(document.getElementById(DEFAULT_ACTIVE_BUTTON_ID));
});
function arrayMax(arr) {
  return arr.reduce(function (p, v) {
    return p > v ? p : v;
  });
}
